package logger

import (
	"context"
	"os"

	"bitbucket.org/vivhealth/viv-admin-mvp/core"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

const (
	loggerKey = iota
	requestIDKey

	requestURIKey    = "uri"
	requestMethodKey = "method"
)

func NewLogger(config *zap.Config) (*zap.Logger, error) {
	if config == nil {
		defaultConfig := zap.NewProductionConfig()
		config = &defaultConfig
		config.OutputPaths = []string{"stdout"}
	}
	config.EncoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	config.Sampling = nil
	config.DisableCaller = true
	config.InitialFields = map[string]interface{}{"pid": os.Getpid()}

	return config.Build()
}

func ToContext(ctx context.Context, logger *zap.Logger) context.Context {
	return context.WithValue(ctx, loggerKey, logger)
}

func PopulateContextWithRequestID(ctx context.Context, requestID core.ID) context.Context {
	return context.WithValue(ctx, requestIDKey, requestID.String())
}

func FromContext(ctx context.Context) *zap.SugaredLogger {
	logger, ok := ctx.Value(loggerKey).(*zap.Logger)
	if !ok {
		panic("Can't get logger")
	}

	if ctxRequestMethod, ok := ctx.Value(requestMethodKey).(string); ok {
		logger = logger.With(zap.String(requestMethodKey, ctxRequestMethod))
	}

	if ctxRequestURI, ok := ctx.Value(requestURIKey).(string); ok {
		logger = logger.With(zap.String(requestURIKey, ctxRequestURI))
	}

	if ctxRequestID, ok := ctx.Value(requestIDKey).(string); ok {
		logger = logger.With(zap.String("request_id", ctxRequestID))
	}

	return logger.Sugar()
}

func Must(logger *zap.Logger, err error) *zap.Logger {
	if err != nil {
		panic(err)
	}

	return logger
}
