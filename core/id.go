package core

import (
	"encoding/json"
	"strings"

	"github.com/pborman/uuid"
	"github.com/pkg/errors"
)

type ID string

func NewID() ID {
	return ID(uuid.New())
}

func ParseID(s string) (ID, bool) {
	id := uuid.Parse(s)
	if id == nil {
		return "", false
	}

	return ID(id.String()), true
}

func (id ID) String() string {
	return string(id)
}

func (id ID) Key() ID {
	return ID(strings.ToLower(string(id)))
}

func (id ID) ExternalKey() ID {
	return ID(strings.ToUpper(string(id)))
}

func (id *ID) UnmarshalJSON(data []byte) error {
	var idStr string
	if err := json.Unmarshal(data, &idStr); err != nil {
		return err
	}

	parsedID, ok := ParseID(idStr)
	if !ok {
		return errors.Errorf("id must be valid UUID: %q", idStr)
	}

	*id = parsedID

	return nil
}

func (id ID) MarshalJSON() ([]byte, error) {
	buf := make([]byte, 0, len(id)+2)
	buf = append(buf, '"')
	buf = append(buf, id...)
	buf = append(buf, '"')

	return buf, nil
}
