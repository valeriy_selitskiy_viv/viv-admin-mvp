package core

import (
	"encoding/json"
	"net/mail"
	"strings"

	"github.com/pkg/errors"
)

// StrictEmail represents a single RFC 5322 address of the form "foo@example.com"
// and force to trim, lower case and forbid address name.
type StrictEmail string

func (email StrictEmail) String() string {
	return string(email)
}

// Normalize trims spaces, lowers case and formats the address as a valid RFC 5322 address without outer "<" and ">".
func (email StrictEmail) Normalize() (StrictEmail, error) {
	em := strings.ToLower(strings.TrimSpace(string(email)))

	addr, err := mail.ParseAddress(em)
	if err != nil {
		return "", errors.Wrapf(err, "email is not valid")
	} else if strings.HasSuffix(em, ">") {
		return "", errors.New("email has to be without outer '<' and '>'")
	}
	normalizedEmail := addr.String()

	return StrictEmail(normalizedEmail[1 : len(normalizedEmail)-1]), nil
}

func (email StrictEmail) validate() error {
	rawEmail := email.String()

	if strings.ContainsAny(rawEmail, "<>&") {
		return errors.New("contains unsafe characters")
	}

	if _, err := email.Normalize(); err != nil {
		return err
	}

	return nil
}

func (email StrictEmail) Validate() error {
	return email.validate()
}

func (email StrictEmail) ValidateJSON() error {
	return email.validate()
}

func (email *StrictEmail) UnmarshalJSON(b []byte) error {
	var em string
	if err := json.Unmarshal(b, &em); err != nil {
		return errors.Wrapf(err, "failed to unmarshal StrictEmail")
	}

	// If normalize is ok then email has to be in strict form
	// If normalize is not ok then email can be in any form and will be validated later
	// In this case we get bad request
	rawEmail := StrictEmail(em)
	normalizedEmail, err := rawEmail.Normalize()
	if err != nil {
		*email = rawEmail
		return nil
	}

	*email = normalizedEmail

	return nil
}
