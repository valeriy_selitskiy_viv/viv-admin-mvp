package model

type ProgramCategoryType string

const (
	ProgramCategoryTypeIntro            ProgramCategoryType = "intro"
	ProgramCategoryTypeActivity         ProgramCategoryType = "activity"
	ProgramCategoryTypeSleep            ProgramCategoryType = "sleep"
	ProgramCategoryTypeNutrition        ProgramCategoryType = "nutrition"
	ProgramCategoryTypeWomenHealth      ProgramCategoryType = "women_health"
	ProgramCategoryTypeStressManagement ProgramCategoryType = "stress_management"
	ProgramCategoryTypeBody             ProgramCategoryType = "body"
	ProgramCategoryTypeSocialTies       ProgramCategoryType = "social_ties"
	ProgramCategoryTypeMindfulness      ProgramCategoryType = "mindfulness"
)
