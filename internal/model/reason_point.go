package model

type ReasonPoint struct {
	Title string `json:"title,omitempty"`
	URL   string `json:"url,omitempty"`
}
