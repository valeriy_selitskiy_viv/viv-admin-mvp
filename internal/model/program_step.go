package model

import (
	"bitbucket.org/vivhealth/viv-admin-mvp/core"
)

type ProgramStep struct {
	ProgramStepID     core.ID           `json:"program_step_id"`
	Title             string            `json:"title,omitempty"`
	Subtitle          string            `json:"subtitle,omitempty"`
	EstimatedDuration int               `json:"estimated_duration,omitempty"`
	ArticleID         core.ID           `json:"article_id"`
	QuizID            core.ID           `json:"quiz_id"`
	QuizGoalID        core.ID           `json:"quiz_goal_id"`
	RelatedPrograms   []*RelatedProgram `json:"related_programs,omitempty"`
}
