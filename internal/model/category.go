package model

import (
	"bitbucket.org/vivhealth/viv-admin-mvp/core"
	"database/sql/driver"
	"encoding/json"

	"github.com/pkg/errors"
)

type Category struct {
	CategoryID       core.ID         `json:"category_id,omitempty"`
	Type             CategoryType    `json:"type,omitempty"`
	Title            string          `json:"title,omitempty"`
	Weight           int             `json:"weight"`
	DynamicProgramID *core.ID        `json:"dynamic_program_id,omitempty"`
	ProgramWeights   map[core.ID]int `json:"program_weights,omitempty"`
}

func (category *Category) Scan(value interface{}) error {
	var data []byte
	switch v := value.(type) {
	case []byte:
		data = v
	case string:
		data = []byte(v)
	default:
		return errors.Errorf("can't convert: %q to Category", v)
	}

	return json.Unmarshal(data, category)
}

func (category Category) Value() (driver.Value, error) {
	return json.Marshal(category)
}
