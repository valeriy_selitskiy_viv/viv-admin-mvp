package model

type ProgramStatus string

const (
	ProgramStatusNew       ProgramStatus = "new"
	ProgramStatusActive    ProgramStatus = "active"
	ProgramStatusCompleted ProgramStatus = "completed"
)

func (status ProgramStatus) IsNew() bool {
	return status == ProgramStatusNew
}

func (status ProgramStatus) IsActive() bool {
	return status == ProgramStatusActive
}

func (status ProgramStatus) IsCompleted() bool {
	return status == ProgramStatusCompleted
}

func (status ProgramStatus) In(statuses []ProgramStatus) bool {
	for _, s := range statuses {
		if s == status {
			return true
		}
	}

	return false
}
