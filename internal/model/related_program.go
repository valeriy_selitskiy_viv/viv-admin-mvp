package model

import (
	"bitbucket.org/vivhealth/viv-admin-mvp/core"
)

type RelatedProgram struct {
	ProgramID core.ID `json:"program_id"`
	Title     string  `json:"title,omitempty"`
	Text      string  `json:"text,omitempty"`
}
