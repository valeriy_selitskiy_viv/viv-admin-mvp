package model

type CategoryType string

const (
	CategoryTypeActivity    CategoryType = "activity"
	CategoryTypeBody        CategoryType = "body"
	CategoryTypeMindfulness CategoryType = "mindfulness"
	CategoryTypeNutrition   CategoryType = "nutrition"
	CategoryTypeSleep       CategoryType = "sleep"
	CategoryTypeSocialTies  CategoryType = "social_ties"
	CategoryTypeWomenHealth CategoryType = "women_health"
)
