package model

type ProgramStepStatus string

const (
	ProgramStepStatusNew         ProgramStepStatus = "new"
	ProgramStepStatusUnavailable ProgramStepStatus = "unavailable"
	ProgramStepStatusActive      ProgramStepStatus = "active"
	ProgramStepStatusCompleted   ProgramStepStatus = "completed"
	ProgramStepStatusFailed      ProgramStepStatus = "failed"
)

func (status ProgramStepStatus) IsNew() bool {
	return status == ProgramStepStatusNew
}

func (status ProgramStepStatus) IsUnavailable() bool {
	return status == ProgramStepStatusUnavailable
}

func (status ProgramStepStatus) IsActive() bool {
	return status == ProgramStepStatusActive
}

func (status ProgramStepStatus) IsCompleted() bool {
	return status == ProgramStepStatusCompleted
}

func (status ProgramStepStatus) IsFailed() bool {
	return status == ProgramStepStatusFailed
}
