package model

type Media struct {
	Type            MediaType `json:"type"`
	URL             string    `json:"url,omitempty"`
	Thumbnail       string    `json:"thumbnail,omitempty"`
	Title           string    `json:"title,omitempty"`
	BackgroundColor string    `json:"background_color,omitempty"`
	Ratio           string    `json:"ratio,omitempty"`
}
