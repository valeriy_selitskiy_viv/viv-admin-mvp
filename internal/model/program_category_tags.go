package model

type ProgramCategoryTags struct {
	Type ProgramCategoryType `json:"type"`
	Tags []string            `json:"tags"`
}
