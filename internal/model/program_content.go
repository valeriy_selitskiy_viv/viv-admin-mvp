package model

type ProgramContent struct {
	Title      string   `json:"title,omitempty"`
	Text       string   `json:"text,omitempty"`
	MediaItems []*Media `json:"media_items,omitempty"`
}
