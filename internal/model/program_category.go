package model

type ProgramCategory struct {
	Type  ProgramCategoryType `json:"type,omitempty"`
	Title string              `json:"title,omitempty"`
}
