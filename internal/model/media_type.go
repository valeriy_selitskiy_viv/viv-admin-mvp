package model

type MediaType string

const (
	MediaTypeAudio MediaType = "audio"
	MediaTypeImage MediaType = "image"
	MediaTypeVideo MediaType = "video"
)
