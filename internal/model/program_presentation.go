package model

type ProgramPresentation struct {
	Title           string `json:"title,omitempty"`
	Thumbnail       string `json:"thumbnail,omitempty"`
	BackgroundColor string `json:"background_color,omitempty"`
	SmallThumbnail  string `json:"small_thumbnail,omitempty"`
}
