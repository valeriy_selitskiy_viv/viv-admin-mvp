package model

import (
	"database/sql/driver"
	"encoding/json"

	"bitbucket.org/vivhealth/viv-admin-mvp/core"
	"github.com/pkg/errors"
)

const (
	IntroProgramTag string = "intro"
)

type Program struct {
	ProgramID        core.ID              `json:"program_id"`
	Category         *ProgramCategory     `json:"category,omitempty"`
	Presentation     *ProgramPresentation `json:"presentation,omitempty"`
	Promotion        *ProgramPromotion    `json:"promotion,omitempty"`
	Content          *ProgramContent      `json:"content,omitempty"`
	Reason           *ProgramReason       `json:"reason,omitempty"`
	SectionTitle     string               `json:"section_title,omitempty"`
	Steps            []*ProgramStep       `json:"steps,omitempty"`
	GoalIDs          []core.ID            `json:"goal_ids,omitempty"`
	Weight           int                  `json:"weight,omitempty"`
	Tags             []string             `json:"tags,omitempty"`
	Free             bool                 `json:"free,omitempty"`
	PendingToRelease bool                 `json:"pending_to_release,omitempty"`
	UserAttributes   *UserAttributes      `json:"user_attributes"`
}

func (program *Program) Scan(value interface{}) error {
	var data []byte
	switch v := value.(type) {
	case []byte:
		data = v
	case string:
		data = []byte(v)
	default:
		return errors.Errorf("can't convert: %q to Program", v)
	}

	return json.Unmarshal(data, program)
}

func (program Program) Value() (driver.Value, error) {
	return json.Marshal(program)
}

func (program Program) IsDynamic() bool {
	return len(program.Steps) == 0
}

func IndexPrograms(programs []*Program) map[core.ID]*Program {
	if programs == nil {
		return nil
	}

	res := make(map[core.ID]*Program)
	for _, program := range programs {
		res[program.ProgramID] = program
	}

	return res
}
