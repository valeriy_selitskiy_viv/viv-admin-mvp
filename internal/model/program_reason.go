package model

type ProgramReason struct {
	Title  string        `json:"title,omitempty"`
	Points []ReasonPoint `json:"points,omitempty"`
}
