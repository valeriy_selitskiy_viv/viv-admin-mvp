package config

import (
	"bitbucket.org/vivhealth/viv-admin-mvp/internal/aws/s3"
)

type ProcessType string

type Config struct {
	S3 s3.Config
}
