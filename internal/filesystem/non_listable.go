package filesystem

import (
	"net/http"
	"strings"
)

type NonListable struct {
	FS http.FileSystem
}

func (nl NonListable) Open(path string) (http.File, error) {
	f, err := nl.FS.Open(path)
	if err != nil {
		return nil, err
	}

	s, err := f.Stat()
	if err == nil && s.IsDir() {
		index := strings.TrimSuffix(path, "/") + "/index.html"
		if _, err := nl.FS.Open(index); err != nil {
			return nil, err
		}
	}

	return f, nil
}
