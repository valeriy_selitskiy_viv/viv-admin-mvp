package httpadapter

import (
	"bitbucket.org/vivhealth/viv-admin-mvp/internal/handler"
)

type Adapter struct {
	service *handler.Service
}

func New(service *handler.Service) *Adapter {
	return &Adapter{
		service: service,
	}
}
