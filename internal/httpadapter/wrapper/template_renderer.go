package wrapper

import (
	"log"
	"net/http"
	"os"

	"github.com/thedevsaddam/renderer"
)

const (
	TemplatesPath = "./resources/tpl"
	DistPath      = "./resources/dist"
)

func TemplateRenderer(h http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		//log.Println(r.URL.Path)
		resp := &fileServerOverrideResponse{
			ResponseWriter: w,
		}
		h.ServeHTTP(resp, r)
		if resp.fsStatus == 404 {
			if _, err := os.OpenFile(TemplatesPath+r.URL.Path, os.O_RDONLY, 0400); err != nil {
				log.Printf("No template %s", r.RequestURI)
				w.WriteHeader(http.StatusNotFound)
				return
			}
			var tpls = []string{
				DistPath + "/layout.html",
				TemplatesPath + "/header.html",
				TemplatesPath + "/footer.html",
				TemplatesPath + "/menu-left.html",
			}

			var path = r.URL.Path
			if path == "/" {
				path = "/dashboard.html"
			}

			tpls = append(tpls, TemplatesPath+path)

			err := renderer.New().Template(w, http.StatusOK, tpls, nil)
			if err != nil {
				log.Fatal(err)
			}
		}
	}
}

type fileServerOverrideResponse struct {
	http.ResponseWriter
	fsStatus int
}

func (w *fileServerOverrideResponse) WriteHeader(httpStatus int) {
	w.fsStatus = httpStatus
	if httpStatus != http.StatusNotFound {
		w.ResponseWriter.WriteHeader(httpStatus)
	}
}

func (w *fileServerOverrideResponse) Write(p []byte) (int, error) {
	if w.fsStatus != http.StatusNotFound {
		return w.ResponseWriter.Write(p)
	}
	return len(p), nil
}
