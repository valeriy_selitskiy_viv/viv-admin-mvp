package httpadapter

import (
	"net/http"
)

func (s *Adapter) GetPrograms(w http.ResponseWriter, r *http.Request) {
	res := s.service.GetPrograms(r.Context())
	if len(res) == 0 {
		responseError(w, nil, "no programs")
		return
	}

	responseOK(w, res)
}
