package httpadapter

import (
	"encoding/json"
	"io"
	"net/http"
)

type ErrorResponse struct {
	StatusCode int                `json:"status_code"`
	Error      *ErrorResponseBody `json:"error,omitempty"`
}

type ErrorResponseBody struct {
	Code    string                `json:"code"`
	Message string                `json:"message,omitempty"`
	Details []*ErrorFieldContract `json:"details,omitempty"`
}

type ErrorFieldContract struct {
	Code    string `json:"code,omitempty"`
	Message string `json:"message,omitempty"`
	Target  string `json:"target,omitempty"`
}

func responseOK(w http.ResponseWriter, res []byte) {
	w.WriteHeader(http.StatusOK)
	w.Write(res)
}

func responseBadRequest(w http.ResponseWriter, message string) {
	res := &ErrorResponse{
		StatusCode: http.StatusBadRequest,
		Error: &ErrorResponseBody{
			Message: message,
		},
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	body, _ := json.MarshalIndent(res, "", "  ")
	w.WriteHeader(res.StatusCode)
	w.Write(body)
}

func responseError(w http.ResponseWriter, res *ErrorResponse, message string) {
	body, _ := json.MarshalIndent(res, "", "  ")
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	if res != nil {
		w.WriteHeader(res.StatusCode)
	}
	w.Write(body)
}

func DecodeBody(body io.ReadCloser, obj interface{}) error {
	if body == nil {
		return nil
	}

	decoder := json.NewDecoder(body)
	if !decoder.More() {
		return nil
	}

	return decoder.Decode(obj)
}
