package httpadapter

import (
	"net/http"
	"strings"
)

func (s *Adapter) GetProgram(w http.ResponseWriter, r *http.Request) {
	key := strings.TrimPrefix(r.URL.Path, "/api/program")
	if len(key) == 0 {
		responseError(w, nil, "no key")
	}
	res := s.service.GetProgram(r.Context(), key)
	if res == nil {
		responseError(w, nil, "no program")
		return
	}

	responseOK(w, res)
}
