package httpadapter

import (
	"net/http"
)

func (s *Adapter) GetCategories(w http.ResponseWriter, r *http.Request) {
	res := s.service.GetCategories(r.Context())
	if len(res) == 0 {
		responseError(w, nil, "no programs")
		return
	}

	responseOK(w, res)
}
