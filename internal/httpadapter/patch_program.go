package httpadapter

import (
	"net/http"
	"strings"

	"bitbucket.org/vivhealth/viv-admin-mvp/internal/handler"
)

func (s *Adapter) PatchProgram(w http.ResponseWriter, r *http.Request) {
	key := strings.TrimPrefix(r.URL.Path, "/api/program")
	if len(key) == 0 {
		responseError(w, nil, "no key")
	}
	var input handler.TaggedProgram
	if err := DecodeBody(r.Body, &input); err != nil {
		responseBadRequest(w, "can't decode body")

		return
	}
	ok, err := s.service.PatchProgram(r.Context(), input, key)
	if err != nil {
		var res = &ErrorResponse{
			StatusCode: 400,
		}

		switch err.Error() {

		case "etag_mismatch":
			res.Error = &ErrorResponseBody{
				Message: "You are trying to update an item, which is outdated, which results in versions conflict. Try refreshing the page and reapply changes.",
			}

		default:
			res.Error = &ErrorResponseBody{
				Message: "Unknown error: " + err.Error(),
			}

		}

		responseError(w, res, "")

		return
	}

	if !ok {
		responseOK(w, []byte("false"))

		return
	}

	responseOK(w, []byte("true"))
}
