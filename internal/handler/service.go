package handler

import (
	"bitbucket.org/vivhealth/viv-admin-mvp/internal/aws/s3"
	"bitbucket.org/vivhealth/viv-admin-mvp/internal/config"
)

type Service struct {
	Cfg config.Config

	DataSource *s3.Client
}
