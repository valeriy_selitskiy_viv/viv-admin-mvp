package handler

import (
	"context"
	"encoding/json"
	"log"

	"bitbucket.org/vivhealth/viv-admin-mvp/internal/model"
)

func (s *Service) GetProgram(ctx context.Context, key string) []byte {
	item, err := s.DataSource.GetObject(ctx, key)
	if err != nil {
		log.Fatalln(err)
	}
	program := model.Program{}
	err = json.Unmarshal(item.Content, &program)
	if err != nil {
		log.Fatalln(err)
	}
	res := &TaggedProgram{
		Program: &program,
		Key:     key,
	}
	err = json.Unmarshal([]byte(*item.Object.ETag), &res.ETag)
	if err != nil {
		log.Fatalln(err)
	}
	out, err := json.MarshalIndent(res, "", "    ")
	if err != nil {
		log.Fatalln(err)
	}

	return out
}
