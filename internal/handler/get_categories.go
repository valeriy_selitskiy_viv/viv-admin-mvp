package handler

import (
	"context"
	"encoding/json"
	"log"

	"bitbucket.org/vivhealth/viv-admin-mvp/internal/model"
)

type TaggedCategory struct {
	Category *model.Category `json:"category"`
	ETag     string          `json:"etag"`
	Key      string          `json:"key"`
}

func (s *Service) GetCategories(ctx context.Context) []byte {

	list, err := s.DataSource.GetCategoriesSet(ctx)
	if err != nil {
		log.Fatalln(err)
	}

	res := make(map[string]*TaggedCategory)
	for key, item := range list {
		category := model.Category{}
		err := json.Unmarshal(item.Content, &category)
		if err != nil {
			log.Fatalln(err)
		}
		res[key] = &TaggedCategory{
			Category: &category,
			Key:      key,
		}
		err = json.Unmarshal([]byte(*item.Object.ETag), &res[key].ETag)
		if err != nil {
			log.Fatalln(err)
		}
	}

	out, err := json.MarshalIndent(res, "", "    ")
	if err != nil {
		log.Fatalln(err)
	}

	return out
}
