package handler

import (
	"context"
	"encoding/json"
	"log"

	"bitbucket.org/vivhealth/viv-admin-mvp/internal/model"
)

type TaggedProgram struct {
	Program *model.Program `json:"program"`
	ETag    string         `json:"etag"`
	Key     string         `json:"key"`
}

func (s *Service) GetPrograms(ctx context.Context) []byte {

	list, err := s.DataSource.GetProgramsSet(ctx)
	if err != nil {
		log.Fatalln(err)
	}

	res := make(map[string]*TaggedProgram)
	for key, item := range list {
		program := model.Program{}
		err := json.Unmarshal(item.Content, &program)
		if err != nil {
			log.Fatalln(err)
		}
		res[key] = &TaggedProgram{
			Program: &program,
			Key:     key,
		}
		err = json.Unmarshal([]byte(*item.Object.ETag), &res[key].ETag)
		if err != nil {
			log.Fatalln(err)
		}
	}

	out, err := json.MarshalIndent(res, "", "    ")
	if err != nil {
		log.Fatalln(err)
	}

	return out
}
