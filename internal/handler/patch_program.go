package handler

import (
	"context"

	"github.com/pkg/errors"
)

func (s *Service) PatchProgram(ctx context.Context, input TaggedProgram, key string) (bool, error) {
	if !s.DataSource.CheckObjectETagValid(ctx, key, input.ETag) {
		return false, errors.New("etag_mismatch")
	}

	return true, nil
}
