package s3

type Config struct {
	Region            string
	ContentBucketName string
}
