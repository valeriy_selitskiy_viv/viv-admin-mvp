package s3

import (
	"bytes"
	"context"
	"log"

	"bitbucket.org/vivhealth/viv-admin-mvp/core/logger"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/pkg/errors"
)

var objectsCache = make(map[string]*Object)

type Object struct {
	Object  *s3.Object `json:"object"`
	Content []byte     `json:"content"`
}

type Client struct {
	s3     *s3.S3
	s3Dldr *s3manager.Downloader
	s3Uldr *s3manager.Uploader
	bucket string
}

func NewClient(s3 *s3.S3, downloader *s3manager.Downloader, uploader *s3manager.Uploader, bucket string) *Client {
	return &Client{
		s3:     s3,
		s3Dldr: downloader,
		s3Uldr: uploader,
		bucket: bucket,
	}
}

func (c *Client) GetProgramsSet(ctx context.Context) (map[string]*Object, error) {

	set := make(map[string]*s3.Object)
	if err := c.s3.ListObjectsV2PagesWithContext(
		ctx,
		&s3.ListObjectsV2Input{
			Bucket: aws.String(c.bucket),
			Prefix: aws.String("programs/"),
		},
		func(page *s3.ListObjectsV2Output, lastPage bool) bool {
			for _, obj := range page.Contents {
				set[*obj.Key] = obj
			}
			return true
		}); err != nil {
		return nil, err
	}

	if len(set) == 0 {
		return nil, errors.New("no programs")
	}

	rawPrograms, err := c.bulkDownload(ctx, set)
	if err != nil {
		return nil, err
	}

	return rawPrograms, nil
}

func (c *Client) GetObject(ctx context.Context, key string) (*Object, error) {
	log := logger.FromContext(ctx)

	object, err := c.s3.GetObjectWithContext(ctx, &s3.GetObjectInput{
		Bucket: aws.String(c.bucket),
		Key:    aws.String(key),
	})
	if err != nil {
		return nil, err
	}
	var res *Object
	if cached, ok := objectsCache[key]; ok && cached != nil {
		if *object.ETag != *cached.Object.ETag {
			delete(objectsCache, key)
		} else {
			log.Debugw("cached Object", "key", key)
			res = cached
		}
	} else {
		log.Debugw("download Object", "key", key)

		s3object := &s3.Object{
			ETag:         object.ETag,
			LastModified: object.LastModified,
			Key:          aws.String(key),
			Size:         object.ContentLength,
			StorageClass: object.StorageClass,
		}
		buf := bytes.NewBuffer([]byte{})
		_, err := buf.ReadFrom(object.Body)
		if err != nil {
			return nil, err
		}

		res = &Object{
			Object:  s3object,
			Content: buf.Bytes(),
		}
	}

	return res, nil
}

func (c *Client) headObject(ctx context.Context, key string) (*s3.HeadObjectOutput, error) {
	headObject, err := c.s3.HeadObjectWithContext(ctx, &s3.HeadObjectInput{
		Bucket: aws.String(c.bucket),
		Key:    aws.String(key),
	})
	if err != nil {
		return nil, err
	}

	return headObject, err
}

func (c *Client) CheckObjectETagValid(ctx context.Context, key, ETag string) bool {
	meta, err := c.headObject(ctx, key)
	if err != nil {
		log.Fatalln(err)
	}

	serverETag := *meta.ETag
	serverETag = serverETag[1 : len(serverETag)-1]

	log.Println(serverETag, ETag)

	return ETag == serverETag
}

func (c *Client) GetCategoriesSet(ctx context.Context) (map[string]*Object, error) {

	set := make(map[string]*s3.Object)
	if err := c.s3.ListObjectsV2PagesWithContext(
		ctx,
		&s3.ListObjectsV2Input{
			Bucket: aws.String(c.bucket),
			Prefix: aws.String("categories/"),
		},
		func(page *s3.ListObjectsV2Output, lastPage bool) bool {
			for _, obj := range page.Contents {
				set[*obj.Key] = obj
			}
			return true
		}); err != nil {
		return nil, err
	}

	if len(set) == 0 {
		return nil, errors.New("no categories")
	}

	rawCategories, err := c.bulkDownload(ctx, set)
	if err != nil {
		return nil, err
	}

	return rawCategories, nil
}

func (c *Client) bulkDownload(ctx context.Context, objects map[string]*s3.Object) (map[string]*Object, error) {
	log := logger.FromContext(ctx)

	res := make(map[string]*Object)
	list := make([]s3manager.BatchDownloadObject, 0)
	writers := make(map[string]*aws.WriteAtBuffer)

	for key, object := range objects {
		if cached, ok := objectsCache[key]; ok && cached != nil {
			if *object.ETag != *cached.Object.ETag {
				delete(objectsCache, key)
			} else {
				log.Debugw("bulk cached Object", "key", key)
				res[key] = cached
			}
		}
		if res[key] == nil {
			log.Debugw("bulk download Object", "key", key)
			writers[key] = aws.NewWriteAtBuffer([]byte{})
			list = append(list, s3manager.BatchDownloadObject{
				Writer: writers[key],
				Object: &s3.GetObjectInput{
					Key:    aws.String(key),
					Bucket: aws.String(c.bucket),
				},
			})
		}
	}

	if len(writers) > 0 {
		if err := c.s3Dldr.DownloadWithIterator(ctx, &s3manager.DownloadObjectsIterator{Objects: list}); err != nil {
			return nil, err
		}
		for key, writer := range writers {
			object := &Object{
				Object:  objects[key],
				Content: writer.Bytes(),
			}

			objectsCache[key] = object
			res[key] = object
		}
	}

	return res, nil
}
