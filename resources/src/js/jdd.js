'use strict';

import {parserJson} from "./jsl/jsl.parser";


function getType(value) {
    if ((function () {
        return value && (value !== this);
    }).call(value)) {
        //fallback on 'typeof' for truthy primitive values
        return typeof value;
    }
    return ({}).toString.call(value).match(/\s([a-z|A-Z]+)/)[1].toLowerCase();
}

function forEach(array, callback, scope) {
    for (let idx = 0; idx < array.length; idx++) {
        callback.call(scope, array[idx], idx, array);
    }
}

export function getJdd() {
    return {

        LEFT: 'left',
        RIGHT: 'right',

        EQUALITY: 'eq',
        TYPE: 'type',
        MISSING: 'missing',
        diffs: [],
        requestCount: 0,

        findDiffs: function (/*Object*/ config1, /*Object*/ data1, /*Object*/ config2, /*Object*/ data2) {
            let jdd = this;

            config1.currentPath.push('/');
            config2.currentPath.push('/');

            let key;
            // no un-used lets
            // let val;

            if (data1.length < data2.length) {
                /*
                 * This means the second data has more properties than the first.
                 * We need to find the extra ones and create diffs for them.
                 */
                for (key in data2) {
                    if (data2.hasOwnProperty(key)) {
                        // no un-used lets
                        // val = data1[key];
                        if (!data1.hasOwnProperty(key)) {
                            jdd.diffs.push(jdd.generateDiff(config1, jdd.generatePath(config1),
                                config2, jdd.generatePath(config2, '/' + key),
                                'The right side of this object has more items than the left side', jdd.MISSING));
                        }
                    }
                }
            }

            /*
             * Now we're going to look for all the properties in object one and
             * compare them to object two
             */
            for (key in data1) {
                if (data1.hasOwnProperty(key)) {
                    // no un-used lets
                    // val = data1[key];

                    config1.currentPath.push(key);

                    if (!data2.hasOwnProperty(key)) {
                        /*
                         * This means that the first data has a property which
                         * isn't present in the second data
                         */
                        jdd.diffs.push(jdd.generateDiff(config1, jdd.generatePath(config1),
                            config2, jdd.generatePath(config2),
                            'Missing property <code>' + key + '</code> from the object on the right side', jdd.MISSING));
                    } else {
                        config2.currentPath.push(key);

                        jdd.diffVal(data1[key], config1, data2[key], config2);
                        config2.currentPath.pop();
                    }
                    config1.currentPath.pop();
                }
            }

            config1.currentPath.pop();
            config2.currentPath.pop();

            /*
             * Now we want to look at all the properties in object two that
             * weren't in object one and generate diffs for them.
             */
            for (key in data2) {
                if (data2.hasOwnProperty(key)) {
                    // no un-used lets
                    // val = data1[key];

                    if (!data1.hasOwnProperty(key)) {
                        jdd.diffs.push(jdd.generateDiff(config1, jdd.generatePath(config1),
                            config2, jdd.generatePath(config2, key),
                            'Missing property <code>' + key + '</code> from the object on the left side', jdd.MISSING));
                    }
                }
            }
        },

        /**
         * Generate the differences between two values.  This handles differences of object
         * types and actual values.
         */
        diffVal: function (val1, config1, val2, config2) {
            let jdd = this;

            if (getType(val1) === 'array') {
                jdd.diffArray(val1, config1, val2, config2);
            } else if (getType(val1) === 'object') {
                if (['array', 'string', 'number', 'boolean', 'null'].indexOf(getType(val2)) > -1) {
                    jdd.diffs.push(jdd.generateDiff(config1, jdd.generatePath(config1),
                        config2, jdd.generatePath(config2),
                        'Both types should be objects', jdd.TYPE));
                } else {
                    jdd.findDiffs(config1, val1, config2, val2);
                }
            } else if (getType(val1) === 'string') {
                if (getType(val2) !== 'string') {
                    jdd.diffs.push(jdd.generateDiff(config1, jdd.generatePath(config1),
                        config2, jdd.generatePath(config2),
                        'Both types should be strings', jdd.TYPE));
                } else if (val1 !== val2) {
                    jdd.diffs.push(jdd.generateDiff(config1, jdd.generatePath(config1),
                        config2, jdd.generatePath(config2),
                        'Both sides should be equal strings', jdd.EQUALITY));
                }
            } else if (getType(val1) === 'number') {
                if (getType(val2) !== 'number') {
                    jdd.diffs.push(jdd.generateDiff(config1, jdd.generatePath(config1),
                        config2, jdd.generatePath(config2),
                        'Both types should be numbers', jdd.TYPE));
                } else if (val1 !== val2) {
                    jdd.diffs.push(jdd.generateDiff(config1, jdd.generatePath(config1),
                        config2, jdd.generatePath(config2),
                        'Both sides should be equal numbers', jdd.EQUALITY));
                }
            } else if (getType(val1) === 'boolean') {
                jdd.diffBool(val1, config1, val2, config2);
            } else if (getType(val1) === 'null' && getType(val2) !== 'null') {
                jdd.diffs.push(jdd.generateDiff(config1, jdd.generatePath(config1),
                    config2, jdd.generatePath(config2),
                    'Both types should be nulls', jdd.TYPE));
            }
        },

        /**
         * Arrays are more complex because we need to recurse into them and handle different length
         * issues so we handle them specially in this function.
         */
        diffArray: function (val1, config1, val2, config2) {
            let jdd = this;

            if (getType(val2) !== 'array') {
                jdd.diffs.push(jdd.generateDiff(config1, jdd.generatePath(config1),
                    config2, jdd.generatePath(config2),
                    'Both types should be arrays', jdd.TYPE));
                return;
            }

            if (val1.length < val2.length) {
                /*
                 * Then there were more elements on the right side and we need to
                 * generate those differences.
                 */
                for (let i = val1.length; i < val2.length; i++) {
                    jdd.diffs.push(jdd.generateDiff(config1, jdd.generatePath(config1),
                        config2, jdd.generatePath(config2, '[' + i + ']'),
                        'Missing element <code>' + i + '</code> from the array on the left side', jdd.MISSING));
                }
            }
            val1.forEach(function (arrayVal, index) {
                if (val2.length <= index) {
                    jdd.diffs.push(jdd.generateDiff(config1, jdd.generatePath(config1, '[' + index + ']'),
                        config2, jdd.generatePath(config2),
                        'Missing element <code>' + index + '</code> from the array on the right side', jdd.MISSING));
                } else {
                    config1.currentPath.push('/[' + index + ']');
                    config2.currentPath.push('/[' + index + ']');

                    if (getType(val2) === 'array') {
                        /*
                         * If both sides are arrays then we want to diff them.
                         */
                        jdd.diffVal(val1[index], config1, val2[index], config2);
                    }
                    config1.currentPath.pop();
                    config2.currentPath.pop();
                }
            });
        },

        /**
         * We handle boolean values specially because we can show a nicer message for them.
         */
        diffBool: function (val1, config1, val2, config2) {
            let jdd = this;

            if (getType(val2) !== 'boolean') {
                jdd.diffs.push(jdd.generateDiff(config1, jdd.generatePath(config1),
                    config2, jdd.generatePath(config2),
                    'Both types should be booleans', jdd.TYPE));
            } else if (val1 !== val2) {
                if (val1) {
                    jdd.diffs.push(jdd.generateDiff(config1, jdd.generatePath(config1),
                        config2, jdd.generatePath(config2),
                        'The left side is <code>true</code> and the right side is <code>false</code>', jdd.EQUALITY));
                } else {
                    jdd.diffs.push(jdd.generateDiff(config1, jdd.generatePath(config1),
                        config2, jdd.generatePath(config2),
                        'The left side is <code>false</code> and the right side is <code>true</code>', jdd.EQUALITY));
                }
            }
        },

        formatAndDecorate: function (/*Object*/ config, /*Object*/ data) {
            let jdd = this;

            if (getType(data) === 'array') {
                jdd.formatAndDecorateArray(config, data);
                return;
            }

            jdd.startObject(config);
            config.currentPath.push('/');

            let props = jdd.getSortedProperties(data);

            /*
             * If the first set has more than the second then we will catch it
             * when we compare values.  However, if the second has more then
             * we need to catch that here.
             */
            props.forEach(function (key) {
                config.out += jdd.newLine(config) + jdd.getTabs(config.indent) + '"' + jdd.unescapeString(key) + '": ';
                config.currentPath.push(key);
                config.paths.push({
                    path: jdd.generatePath(config),
                    line: config.line
                });
                jdd.formatVal(data[key], config);
                config.currentPath.pop();
            });

            jdd.finishObject(config);
            config.currentPath.pop();
        },

        /**
         * Format the array into the output stream and decorate the data tree with
         * the data about this object.
         */
        formatAndDecorateArray: function (/*Object*/ config, /*Array*/ data) {
            let jdd = this;

            jdd.startArray(config);

            /*
             * If the first set has more than the second then we will catch it
             * when we compare values.  However, if the second has more then
             * we need to catch that here.
             */
            data.forEach(function (arrayVal, index) {
                config.out += jdd.newLine(config) + jdd.getTabs(config.indent);
                config.paths.push({
                    path: jdd.generatePath(config, '[' + index + ']'),
                    line: config.line
                });

                config.currentPath.push('/[' + index + ']');
                jdd.formatVal(arrayVal, config);
                config.currentPath.pop();
            });

            jdd.finishArray(config);
            config.currentPath.pop();
        },

        /**
         * Generate the start of the an array in the output stream and push in the new path
         */
        startArray: function (config) {
            let jdd = this;

            config.indent++;
            config.out += '[';

            if (config.paths.length === 0) {
                /*
                 * Then we are at the top of the array and we want to add
                 * a path for it.
                 */
                config.paths.push({
                    path: jdd.generatePath(config),
                    line: config.line
                });
            }

            if (config.indent === 0) {
                config.indent++;
            }
        },

        /**
         * Finish the array, outdent, and pop off all the path
         */
        finishArray: function (config) {
            let jdd = this;

            if (config.indent === 0) {
                config.indent--;
            }

            jdd.removeTrailingComma(config);

            config.indent--;
            config.out += jdd.newLine(config) + jdd.getTabs(config.indent) + ']';
            if (config.indent !== 0) {
                config.out += ',';
            } else {
                config.out += jdd.newLine(config);
            }
        },

        /**
         * Generate the start of the an object in the output stream and push in the new path
         */
        startObject: function (config) {
            let jdd = this;

            config.indent++;
            config.out += '{';

            if (config.paths.length === 0) {
                /*
                 * Then we are at the top of the object and we want to add
                 * a path for it.
                 */
                config.paths.push({
                    path: jdd.generatePath(config),
                    line: config.line
                });
            }

            if (config.indent === 0) {
                config.indent++;
            }
        },

        /**
         * Finish the object, outdent, and pop off all the path
         */
        finishObject: function (config) {
            let jdd = this;

            if (config.indent === 0) {
                config.indent--;
            }

            jdd.removeTrailingComma(config);

            config.indent--;
            config.out += jdd.newLine(config) + jdd.getTabs(config.indent) + '}';
            if (config.indent !== 0) {
                config.out += ',';
            } else {
                config.out += jdd.newLine(config);
            }
        },

        /**
         * Format a specific value into the output stream.
         */
        formatVal: function (val, config) {
            let jdd = this;

            if (getType(val) === 'array') {
                config.out += '[';

                config.indent++;
                val.forEach(function (arrayVal, index) {
                    config.out += jdd.newLine(config) + jdd.getTabs(config.indent);
                    config.paths.push({
                        path: jdd.generatePath(config, '[' + index + ']'),
                        line: config.line
                    });

                    config.currentPath.push('/[' + index + ']');
                    jdd.formatVal(arrayVal, config);
                    config.currentPath.pop();
                });
                jdd.removeTrailingComma(config);
                config.indent--;

                config.out += jdd.newLine(config) + jdd.getTabs(config.indent) + ']' + ',';
            } else if (getType(val) === 'object') {
                jdd.formatAndDecorate(config, val);
            } else if (getType(val) === 'string') {
                config.out += '"' + jdd.unescapeString(val) + '",';
            } else if (getType(val) === 'number') {
                config.out += val + ',';
            } else if (getType(val) === 'boolean') {
                config.out += val + ',';
            } else if (getType(val) === 'null') {
                config.out += 'null,';
            }
        },

        /**
         * When we parse the JSON string we end up removing the escape strings when we parse it
         * into objects.  This results in invalid JSON if we insert those strings back into the
         * generated JSON.  We also need to look out for characters that change the line count
         * like new lines and carriage returns.
         *
         * This function puts those escaped values back when we generate the JSON output for the
         * well known escape strings in JSON.  It handles properties and values.
         *
         * This function does not handle unicode escapes.  Unicode escapes are optional in JSON
         * and the JSON output is still valid with a unicode character in it.
         */
        unescapeString: function (val) {
            if (val) {
                return val.replace('\\', '\\\\')    // Single slashes need to be replaced first
                    .replace(/\"/g, '\\"')     // Then double quotes
                    .replace(/\n/g, '\\n')     // New lines
                    .replace('\b', '\\b')      // Backspace
                    .replace(/\f/g, '\\f')     // Formfeed
                    .replace(/\r/g, '\\r')     // Carriage return
                    .replace(/\t/g, '\\t');    // Horizontal tabs
            } else {
                return val;
            }
        },

        /**
         * Generate a JSON path based on the specific configuration and an optional property.
         */
        generatePath: function (config, prop) {
            let s = '';
            config.currentPath.forEach(function (path) {
                s += path;
            });

            if (prop) {
                s += '/' + prop;
            }

            if (s.length === 0) {
                return '/';
            } else {
                return s;
            }
        },

        /**
         * Add a new line to the output stream
         */
        newLine: function (config) {
            config.line++;
            return '\n';
        },

        /**
         * Sort all the relevant properties and return them in an alphabetical sort by property key
         */
        getSortedProperties: function (/*Object*/ obj) {
            let props = [];

            for (let prop in obj) {
                if (obj.hasOwnProperty(prop)) {
                    props.push(prop);
                }
            }

            props = props.sort(function (a, b) {
                return a.localeCompare(b);
            });

            return props;
        },

        /**
         * Generate the diff and verify that it matches a JSON path
         */
        generateDiff: function (config1, path1, config2, path2, /*String*/ msg, type) {
            if (path1 !== '/' && path1.charAt(path1.length - 1) === '/') {
                path1 = path1.substring(0, path1.length - 1);
            }

            if (path2 !== '/' && path2.charAt(path2.length - 1) === '/') {
                path2 = path2.substring(0, path2.length - 1);
            }
            let pathObj1 = config1.paths.find(function (path) {
                return path.path === path1;
            });
            let pathObj2 = config2.paths.find(function (path) {
                return path.path === path2;
            });

            if (!pathObj1) {
                throw 'Unable to find line number for (' + msg + '): ' + path1;
            }

            if (!pathObj2) {
                throw 'Unable to find line number for (' + msg + '): ' + path2;
            }

            return {
                path1: pathObj1,
                path2: pathObj2,
                type: type,
                msg: msg
            };
        },

        /**
         * Get the current indent level
         */
        getTabs: function (/*int*/ indent) {
            let s = '';
            for (let i = 0; i < indent; i++) {
                s += '    ';
            }

            return s;
        },

        /**
         * Remove the trailing comma from the output.
         */
        removeTrailingComma: function (config) {
            /*
             * Remove the trailing comma
             */
            if (config.out.charAt(config.out.length - 1) === ',') {
                config.out = config.out.substring(0, config.out.length - 1);
            }
        },

        /**
         * Create a config object for holding differences
         */
        createConfig: function () {
            return {
                out: '',
                indent: -1,
                currentPath: [],
                paths: [],
                line: 1
            };
        },

        /**
         * Format the output pre tags.
         */
        formatPRETags: function () {
            forEach($('pre'), function (pre) {
                let codeBlock = $('<pre class="codeBlock"></pre>');
                let lineNumbers = $('<div class="gutter"></div>');
                codeBlock.append(lineNumbers);

                let codeLines = $('<div></div>');
                codeBlock.append(codeLines);

                let addLine = function (line, index) {
                    let div = $('<div class="codeLine line' + (index + 1) + '"></div>');
                    lineNumbers.append($('<span class="line-number">' + (index + 1) + '.</span>'));

                    let span = $('<span class="code"></span>');
                    span.text(line);
                    div.append(span);

                    codeLines.append(div);
                };

                let lines = $(pre).text().split('\n');
                lines.forEach(addLine);

                codeBlock.addClass($(pre).attr('class'));
                codeBlock.attr('id', $(pre).attr('id'));

                $(pre).replaceWith(codeBlock);
            });
        },

        handleDiffClick: function (line, side) {
            let jdd = this;

            let diffs = jdd.diffs.filter(function (diff) {
                if (side === jdd.LEFT) {
                    return line === diff.path1.line;
                } else if (side === jdd.RIGHT) {
                    return line === diff.path2.line;
                } else {
                    return line === diff.path1.line || line === diff.path2.line;
                }
            });

            $('pre.left span.code').removeClass('selected');
            $('pre.right span.code').removeClass('selected');
            diffs.forEach(function (diff) {
                $('pre.left div.line' + diff.path1.line + ' span.code').addClass('selected');
                $('pre.right div.line' + diff.path2.line + ' span.code').addClass('selected');
            });

            if (side === jdd.LEFT || side === jdd.RIGHT) {
                jdd.currentDiff = jdd.diffs.findIndex(function (diff) {
                    return diff.path1.line === line;
                });
            }

            if (jdd.currentDiff === -1) {
                jdd.currentDiff = jdd.diffs.findIndex(function (diff) {
                    return diff.path2.line === line;
                });
            }
        },

        highlightPrevDiff: function () {
            let jdd = this;

            if (jdd.currentDiff > 0) {
                jdd.currentDiff--;
                jdd.highlightDiff(jdd.currentDiff);
                jdd.scrollToDiff(jdd.diffs[jdd.currentDiff]);
            }
        },

        highlightNextDiff: function () {
            let jdd = this;

            if (jdd.currentDiff < jdd.diffs.length - 1) {
                jdd.currentDiff++;
                jdd.highlightDiff(jdd.currentDiff);
                jdd.scrollToDiff(jdd.diffs[jdd.currentDiff]);
            }
        },

        /**
         * Highlight the diff at the specified index
         */
        highlightDiff: function (index) {
            let jdd = this;

            jdd.handleDiffClick(jdd.diffs[index].path1.line, jdd.BOTH);
        },

        /**
         * Scroll the specified diff to be visible
         */
        scrollToDiff: function (diff) {
            $('html, body').animate({
                scrollTop: $('pre.left div.line' + diff.path1.line + ' span.code').offset().top
            }, 0);
        },

        /**
         * Process the specified diff
         */
        processDiffs: function () {
            let left = [];
            let right = [];
            let jdd = this;

            jdd.diffs.forEach(function (diff) {
                $('pre.left div.line' + diff.path1.line + ' span.code').addClass(diff.type).addClass('diff');
                if (left.indexOf(diff.path1.line) === -1) {
                    $('pre.left div.line' + diff.path1.line + ' span.code').click(function () {
                        jdd.handleDiffClick(diff.path1.line, jdd.LEFT);
                    });
                    left.push(diff.path1.line);
                }

                $('pre.right div.line' + diff.path2.line + ' span.code').addClass(diff.type).addClass('diff');
                if (right.indexOf(diff.path2.line) === -1) {
                    $('pre.right div.line' + diff.path2.line + ' span.code').click(function () {
                        jdd.handleDiffClick(diff.path2.line, jdd.RIGHT);
                    });
                    right.push(diff.path2.line);
                }
            });

            jdd.diffs = jdd.diffs.sort(function (a, b) {
                return a.path1.line - b.path1.line;
            });

        },

        /**
         * Validate the input against the JSON parser
         */
        validateInput: function (json, side) {
            let jdd = this;

            try {
                parserJson().parse(json);
                $(side === jdd.LEFT ? '#errorLeft' : '#errorRight').text('').hide();

                return true;
            } catch (parseException) {
                $(side === jdd.LEFT ? '#errorLeft' : '#errorRight').text(parseException.message).show();

                return false;
            }
        },

        /**
         * Generate the report section with the diff
         */
        generateReport: function () {
            let jdd = this;

            let report = $('#report');

            report.text('');

            if (jdd.diffs.length === 0) {
                report.append('<span>The two versions were semantically  identical.</span>');
                return;
            }

            let typeCount = 0;
            let eqCount = 0;
            let missingCount = 0;
            jdd.diffs.forEach(function (diff) {
                if (diff.type === jdd.EQUALITY) {
                    eqCount++;
                } else if (diff.type === jdd.MISSING) {
                    missingCount++;
                } else if (diff.type === jdd.TYPE) {
                    typeCount++;
                }
            });

            let title = $('<div class="reportTitle"></div>');
            if (jdd.diffs.length === 1) {
                title.text('Found ' + (jdd.diffs.length) + ' difference');
            } else {
                title.text('Found ' + (jdd.diffs.length) + ' differences');
            }

            report.prepend(title);
        },

        /**
         * Implement the compare button and complete the compare process
         */
        compare: function (left, right) {
            let jdd = this;


            let leftValid = jdd.validateInput(JSON.stringify(left), jdd.LEFT);
            let rightValid = jdd.validateInput(JSON.stringify(right), jdd.RIGHT);

            if (!leftValid || !rightValid) {
                return;
            }

            $('div.initContainer').hide();
            $('div.diffcontainer').show();

            jdd.diffs = [];

            let config = jdd.createConfig();
            jdd.formatAndDecorate(config, left);
            $('#out').text(config.out);

            let config2 = jdd.createConfig();
            jdd.formatAndDecorate(config2, right);
            $('#out2').text(config2.out);

            jdd.formatPRETags();

            config.currentPath = [];
            config2.currentPath = [];

            jdd.diffVal(left, config, right, config2);
            jdd.processDiffs();
            jdd.generateReport();

            if (jdd.diffs.length > 0) {
                jdd.highlightDiff(0);
                jdd.currentDiff = 0;

            }
        },
    }
}
