// import 'bootstrap'
import $ from 'jquery'
import 'popper.js'
import 'perfect-scrollbar'
import '@coreui/coreui';
import 'datatables.net-dt/css/jquery.datatables.css';
import '@coreui/coreui/dist/css/coreui.min.css';
import 'flag-icon-css/sass/flag-icon.scss';
import 'font-awesome/scss/font-awesome.scss'
import 'simple-line-icons/scss/simple-line-icons.scss'
import '../scss/app.scss';

import {getProgram, getPrograms} from "./handlers/programs";
import {router, routes} from "./router";


$(() => {
    let cover = $('.boverlay');
    $(document)
        .ajaxStart(() => {
            cover.show();
        })
        .ajaxStop(() => {
            cover.hide();
        });

    let matchedRoute = router.lookup(window.location.pathname + window.location.search, 'GET');
    if (!matchedRoute) {
        console.error(`not matched route {window.location.pathname}`)
    }

    switch (matchedRoute.name) {
        case routes.programs:
            getPrograms(matchedRoute.options);
            break;

        case routes.program:
            getProgram(matchedRoute.options);
            break;

        default:
            console.info("no module detected", window.location.pathname);
    }

});

export function uuidv4() {
    return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    )
}