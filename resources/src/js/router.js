import uniloc from "uniloc";

export const routes = {
    programs: 'programs_get',
    program: 'program_get',

    api: {
        programs: {
            get: 'api_programs_get',
        },
        program: {
            get: 'api_program_get',
            post: 'api_program_post',
            patch: 'api_program_patch',
            delete: 'api_program_delete',
        },

        categories: {
            get: 'api_categories_get',
        }
    }
};

export const router = uniloc(
    {
        [routes.programs]: 'GET /programs.html',
        [routes.program]: 'GET /program.html',

        [routes.api.programs.get]: 'GET /api/programs',
        [routes.api.program.get]: 'GET /api/program/:key',
        [routes.api.program.post]: 'POST /api/programs',
        [routes.api.program.patch]: 'PATCH /api/program/:key',
        [routes.api.program.delete]: 'DELETE /api/program/:key',

        [routes.api.categories.get]: 'GET /api/categories',

    },
);
