import $ from 'jquery'
import 'popper.js'
import 'bootstrap'
import 'perfect-scrollbar'
import '@coreui/coreui';
import dt from 'datatables.net';
import 'datatables.net-dt';
import {router, routes} from "../router";
import * as Taggle from "taggle";
import {uuidv4} from "../app";
import 'spectrum-colorpicker';
import '../jsl/jsl.parser'
import '../jsl/jsl.format'
import {getJdd} from "../jdd";
import '../../scss/jdd.css'
import '@coreui/coreui/dist/css/coreui.min.css';
import 'flag-icon-css/sass/flag-icon.scss';
import 'font-awesome/scss/font-awesome.scss'
import 'simple-line-icons/scss/simple-line-icons.scss'
import 'spectrum-colorpicker/spectrum.css';
import '../../scss/app.scss';


// hack do not cleanup unused imports
typeof dt === true;

const colorChanged = 'colorChanged';

let programKey = '';

let tpl = {
    mediaItem: $(),
    step: $(),
    relatedProgram: $(),
    userAttribute: $(),
};
let cleanProgram;

/**
 * @var Taggle $tags
 */
let $tags;

export function getPrograms() {
    $.get(router.generate(routes.api.programs.get), data => {
        let $programsEl = $('#programs');
        let programsDT = $programsEl.DataTable({
            stateSave: true,
            order: [
                [1, 'asc'],
            ],
            columnDefs: [
                {
                    targets: [-1],
                    type: 'html',
                    data: null,
                    defaultContent:
                        '<button type="button" class="edit-program icon-pencil icons btn btn-primary" style="margin: 0 5px" /> ' +
                        '<button type="button" class="delete-program icon-trash btn btn-warning" style="margin: 0 5px" /> '
                },
                {
                    targets: [0],
                    visible: false,
                },

            ],
        });
        programsDT.clear();

        $.each(data, (key, item) => {
            programsDT.row.add(
                [
                    key,
                    item.program.presentation ? item.program.presentation.title : item.program.title,
                    item.program.category.title,
                    (item.program.steps && item.program.steps.length) ? 'Static' : 'Dynamic',
                    item.program.weight,
                ]
            )
        });

        programsDT.draw();

        $programsEl.find('tbody button').on('click', e => {
            let $target = $(e.target);
            let $rowTR = $target.closest('tr');

            switch (true) {
                case $target.hasClass('edit-program'):
                    window.location = router.generate(routes.program, {key: programsDT.row($rowTR).data()[0]});
                    break;

                case $target.hasClass('delete-program'):
                    if (deleteProgram(programsDT.row($rowTR).data()[0])) {
                        programsDT.row($rowTR).remove();
                    } else {
                        alert('not deleted');
                    }
                    break;

                default:
                    console.warn('unknown action');
            }
        });

        let deleteProgram = programId => {
            return false
        }
    });
}


export function getProgram(query) {
    if (!query || !query.key) {
        return false;
    }
    programKey = query.key;

    $.get(router.generate(routes.api.program.get, {key: programKey}), data => {

        cleanProgram = data;

        $tags = new Taggle('tags');
        populateProgramForm(data);
    });
}

async function populateProgramForm(data) {
    let $form = $('#program_form');
    if (!$form.length) {
        throw new Error('no product form');
    }
    tpl.relatedProgram = initRelatedPrograms();
    tpl.step = initSteps();
    tpl.mediaItem = initMediaItems();
    tpl.userAttribute = initUserAttributes();

    spectrumize($(document).find('input[type="color"]'));
    $(document).on(colorChanged, () => {
        $(document).find('input[type="color"]').each((idx, input) => {
            let $plainInput = $('[name="' + $(input).attr("data-for") + '"]', $(input).parents().eq(-1));
            if (!$plainInput.length) {
                return;
            }

            $plainInput.val($(input).spectrum("get").toHexString());
        });
    });

    await $.get(router.generate(routes.api.categories.get), data => {
        let $categories = $form.find('[name="category.type"]');
        $categories.empty();
        $.each(data, (i, row) => {
            $categories.append(
                $('<option>').val(row.category.type).html(row.category.title)
            )
        });
    });

    sectionPopulator('', {program_key: data.key.replace(/\/programs\/|_program\.json/g, '')});
    sectionPopulator('', data.program);

    $(document).on('click', '.generate-uuid', e => {
        let $btn = $(e.target);
        let $input = $btn.closest('.input-group').find('input:first');
        if (!$input.length) {
            alert('No corresponding input found');

            return false;
        }

        let notEmpty = false;
        if ($input.val()) {
            notEmpty = true;
        }

        if (!notEmpty || notEmpty && confirm('Generated UUID will replace existing value, are you sure?')) {
            $input.val(uuidv4());
        }

        return false;
    });

    let $modal = $('.modal-program-diff').removeClass('tpl');

    $('.save-program')
        .removeAttr('disabled')
        .removeClass('btn-light')
        .addClass('btn-primary')
        .on('click', () => {
            return saveProgram($form, $modal);
        });

    $('.save-approved')
        .on('click', () => {
            return pushProgramToServer($form, $modal);
        });

    $('[name="program_key"], [name="program_id"]')
        .attr('readonly', true)
        .addClass('disabled')
        .closest('fieldset').find('.generate-uuid').parent().remove();
    ;
}

function getDirtyProgram($form) {
    let dirtyProgram = serializeForm($form), key = dirtyProgram.program_key;
    delete dirtyProgram.program_key;

    return {
        key: '/programs/' + key + '_program.json',
        program: dirtyProgram,
        etag: cleanProgram.etag
    };
}

function saveProgram($form, $modal) {
    let dirtyProgram = getDirtyProgram($form);

    getJdd().compare(cleanProgram, dirtyProgram);
    $modal.modal('show');

    return false;
}

async function pushProgramToServer($form, $modal) {
    let dirtyProgram = getDirtyProgram($form);
    // let difference = detailedDiff(cleanProgram, dirtyProgram);

    await $.ajax({
        method: 'PATCH',
        url: router.generate(routes.api.program.patch, {key: dirtyProgram.key}),
        processData: false,
        contentType: 'application/json',
        data: JSON.stringify(dirtyProgram),
    }).always((data, status, error) => {
        switch (status) {
            case 'error':
                alert('Something went wrong: ' + error.statusText);
                break;

            case "success":
                //ok
                break;


            default:
                break;

        }

        console.log(data, status, error);
    });

    return true;
}


function serializeForm($form) {
    let plain = {
            user_attributes: null
        },
        plainArray = $form.serializeArray(),
        attributesArray = [];

    $.each(plainArray, (i, v) => {
        let {name, value} = v;
        switch (true) {
            case name === 'free':
                value = value === 'on';

                break;
            case name.search(/_color$/) !== -1:
                value = value.toUpperCase();

                break;
            case name === 'weight':
            case name.search(/_duration$/) !== -1:
                value = value | 0;

                break;
            case name === 'user_attribute_name':
            case name === 'user_attribute_value':
                attributesArray.push(value);
                return true;

                break;
        }

        plain = objectTreeBuilder(plain, name, value);
    });

    if (attributesArray.length) {
        plain['user_attributes'] = {};
        let lastIndex = '';
        $.each(attributesArray, (i, v) => {
            if (lastIndex) {
                plain['user_attributes'][lastIndex] = v;
                lastIndex = '';
            } else {
                lastIndex = v;
            }
        });
    }

    if (plain['taggles'] !== undefined) {
        plain['tags'] = plain['taggles'];
        delete plain['taggles'];
    }

    return plain;
}

function objectTreeBuilder(rootObj, key, value) {
    let idx;
    if (key.search(/_plain$/) !== -1) {
        // just do nothing
    } else if ((idx = key.search(/\./)) !== -1) {
        let prefixKey = key.substr(0, idx),
            nextKey = key.substr(idx + 1),
            arrayPos,
            arrayMatches,
            arrayExpr = /\[(\d+?)]/;
        if (
            (arrayPos = prefixKey.search(arrayExpr))
            && arrayPos !== -1
            && (arrayMatches = prefixKey.match(arrayExpr))
        ) {
            let arrayIdx = parseInt(arrayMatches[1]);
            let arrayPrefix = prefixKey.substr(0, arrayPos | 0);
            if (!(arrayPrefix in rootObj)) {
                rootObj[arrayPrefix] = [];
            }
            if (!(arrayIdx in rootObj[arrayPrefix])) {
                rootObj[arrayPrefix][arrayIdx] = {}
            }

            rootObj[arrayPrefix][arrayIdx] = objectTreeBuilder(rootObj[arrayPrefix][arrayIdx], nextKey, value);
        } else {
            if (!(prefixKey in rootObj)) {
                rootObj[prefixKey] = {}
            }

            rootObj[prefixKey] = objectTreeBuilder(rootObj[prefixKey], nextKey, value);
        }
    } else if (key.search(/\[]$/) !== -1) {
        let shortKey = key.substr(0, key.length - 2);
        if (rootObj[shortKey] === undefined) {
            rootObj[shortKey] = [];
        }
        rootObj[shortKey].push(value);
    } else {
        rootObj[key] = value;
    }

    return rootObj;
}

function spectrumize($colors) {
    $colors.each((idx, input) => {
        let $plainInput = $('[name="' + $(input).attr("data-for") + '"]', $(input).parents().eq(-1));
        if (!$plainInput.length) {
            return;
        }

        $plainInput.on('input', () => {
            $(input).spectrum("set", $plainInput.val());
        });
    });
    $colors.spectrum({
        preferredFormat: "hex",
        showInput: true,
        replacerClassName: 'form-control',
        change: color => {
            // no way to track source so we update all inputs
            $(document).trigger(colorChanged, color);
        }
    });
}

function initSteps() {
    let $stepTpl = $('body').find('.tpl.steps');
    if (!$stepTpl.length) {
        throw new Error('found no steps tpl');
    }
    $stepTpl.detach().removeClass('tpl');

    return $stepTpl;
}

function initMediaItems() {
    let $mediaItemTpl = $('body').find('.tpl.mediaItem');
    if (!$mediaItemTpl.length) {
        throw new Error('found no media item tpl');
    }
    $mediaItemTpl.detach().removeClass('tpl');

    return $mediaItemTpl;
}

function initRelatedPrograms() {
    let $relatedProgram = $('body').find('.tpl.relatedProgram');
    if (!$relatedProgram.length) {
        throw new Error('found no related program tpl');
    }
    $relatedProgram.detach().removeClass('tpl');

    return $relatedProgram;
}

function initUserAttributes() {
    let $userAttribute = $('body').find('.tpl.userAttribute');
    if (!$userAttribute.length) {
        throw new Error('found no user attribute tpl');
    }
    $userAttribute.detach().removeClass('tpl');

    return $userAttribute;
}

function sectionPopulator(sectionName, data) {
    $.each(data, (dataIndex, dataValue) => {
        let elID = (sectionName ? sectionName + '.' : '') + dataIndex;
        let $el = $('[name="' + elID + '"]');
        switch (true) {

            case dataIndex === 'steps':
                stepsPopulator(dataValue, $('.stepsNode'));
                break;

            case dataIndex === 'media_items':
                mediaItemsPopulator(dataValue, $('.mediaItemsNode'));
                break;

            case dataIndex === 'user_attributes':
                userAttributesPopulator(dataValue, $('.userAttributesNode'));
                break;

            case dataIndex === 'tags':
                $.each(dataValue, (i, v) => {
                    $tags.add(v);
                });
                break;

            case dataIndex.search(/_color$/) !== -1:
                $el.val(dataValue);
                $('[name="' + elID + '_plain"]').val(dataValue);
                break;

            case !$.isPlainObject(dataValue) && !!$el.length:
                if ($el.attr('type') === 'checkbox') {
                    $el.prop('checked', !!dataValue);
                } else if ($el.attr('type') === 'file') {
                    // todo s3 file management
                } else {
                    $el.val(dataValue);
                }
                break;

            case $.isPlainObject(dataValue):
                sectionPopulator(dataIndex, dataValue);
                break;

            default:
                console.log('skipping ' + elID);
                break;
        }
    });
}

function mediaItemsPopulator(mediaItems, root) {
    let $tail = root.hide().find('.tpl-tail').detach();
    root.children().not('legend').remove();

    $.each(mediaItems, (idx, mi) => {
        let $mi = tpl.mediaItem.clone();
        let $legend = $mi.find('> legend');
        $legend.html($legend.html().replace('%NITER%', idx + 1));
        $mi.find('[for*="%N%"], [name*="%N%"], [data-for*="%N%"]').each((i, el) => {
            let $el = $(el);
            $el.attr('for', ('' + $el.attr('for')).replace('%N%', idx));
            $el.attr('name', ('' + $el.attr('name')).replace('%N%', idx));
            $el.attr('data-for', ('' + $el.attr('data-for')).replace('%N%', idx));
        });

        root.append($mi);

        sectionPopulator('content.media_items[' + idx + ']', mi);
        spectrumize($mi.find('[name$="background_color"]'));
    });

    root.append($tail);
    root.show();
}

function stepsPopulator(steps, root) {
    let $tail = root.hide().find('.tpl-tail').detach();
    root.children().not('legend').remove();

    $.each(steps, (idx, step) => {
        let $step = tpl.step.clone();
        let $legend = $step.find('> legend');
        $legend.html($legend.html().replace('%NITER%', idx + 1));
        $step.find('[for*="%N%"], [name*="%N%"], [data-for*="%N%"]').each((i, el) => {
            let $el = $(el);
            $el.attr('for', ('' + $el.attr('for')).replace('%N%', idx));
            $el.attr('name', ('' + $el.attr('name')).replace('%N%', idx));
            $el.attr('data-for', ('' + $el.attr('data-for')).replace('%N%', idx));

        });

        relatedProgramsPopulator(step.related_programs, $step.find('.relatedProgramsNode'), idx);
        root.append($step);

        let unrelatedStep = $.extend({}, step);
        delete unrelatedStep.related_programs;

        sectionPopulator('steps[' + idx + ']', unrelatedStep);
    });

    root.append($tail);
    root.show();
}

function relatedProgramsPopulator(relatedPrograms, root, stepID) {
    let $tail = root.hide().find('.tpl-tail').detach();
    root.children().not('legend').remove();

    $.each(relatedPrograms, (idx, rp) => {
        let $rp = tpl.relatedProgram.clone();
        $rp.find('[for*="%N%"], [name*="%N%"], [data-for*="%N%"]').each((i, el) => {
            let $el = $(el);
            $el.attr('for', ('' + $el.attr('for')).replace('%N%', stepID));
            $el.attr('name', ('' + $el.attr('name')).replace('%N%', stepID));
            $el.attr('data-for', ('' + $el.attr('data-for')).replace('%N%', idx));

        });
        $rp.find('[for*="%NN%"], [name*="%NN%"], [data-for*="%N%"]').each((i, el) => {
            let $el = $(el);
            $el.attr('for', ('' + $el.attr('for')).replace('%NN%', idx));
            $el.attr('name', ('' + $el.attr('name')).replace('%NN%', idx));
            $el.attr('data-for', ('' + $el.attr('data-for')).replace('%NN%', idx));
        });

        $rp.find('[name$="program_id"]').val(rp.program_id);
        $rp.find('[name$="title"]').val(rp.title);
        $rp.find('[name$="text"]').val(rp.text);

        root.append($rp);
    });

    root.append($tail);
    root.show();
}

function userAttributesPopulator(attributes, root) {
    let $tail = root.hide().find('.tpl-tail').detach();
    root.children().not('legend').remove();

    $.each(attributes, (attr, val) => {
        let $ua = tpl.userAttribute.clone();

        $ua.find('[name$="user_attribute_name"]').val(attr);
        $ua.find('[name$="user_attribute_value"]').val(val);

        root.append($ua);
    });

    root.append($tail);
    root.show();
}