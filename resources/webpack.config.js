const webpack = require('webpack');
const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HardSourceWebpackPlugin = require('hard-source-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const BUILD_DIR = path.resolve(__dirname, 'dist');
const SRC_DIR = path.resolve(__dirname, 'src');

const config = {
    devtool: 'eval-source-map',
    entry: {
        app: `${SRC_DIR}/js/app.js`,
    },
    output: {
        filename: 'js/[name].[hash:6].js',
        path: BUILD_DIR,
    },
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: ['style-loader', MiniCssExtractPlugin.loader, 'css-loader', 'postcss-loader', 'sass-loader'],
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    {
                        loader: 'css-loader?sourceMap',
                        options: {
                            sourceMap: true
                        }
                    },
                    'postcss-loader?sourceMap'
                ],

            },
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader',
            },
            {
                test: /\.(png|gif|jpg|jpeg)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {name: 'img/[name].[hash:6].[ext]', publicPath: '../', limit: 8192},
                    },
                ],
            },
            {
                test: /\.(woff(2|)|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: 'fonts/[name].[ext]',
                        publicPath: '../',
                    },
                }],
            },
            {
                test: require.resolve('jquery'),
                loader: 'expose-loader?jQuery!expose-loader?$'
            }

        ],
    },
    optimization: {
        minimizer: [
            new UglifyJsPlugin({
                parallel: true,
            }),
            new OptimizeCssAssetsPlugin({
                cssProcessorPluginOptions: {
                    preset: ['default', {discardComments: {removeAll: true}}],
                },
            }),
        ],
    },
    plugins: [
        new CleanWebpackPlugin('dist', {
            exclude: ['img'],
            verbose: true,
            dry: false
        }),
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify('production'),
            },
        }),
        new webpack.optimize.AggressiveMergingPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin(),
        new HardSourceWebpackPlugin(),
        new HtmlWebpackPlugin({
            inject: 'head',
            hash: true,
            template: './src/layout.html',
            filename: 'layout.html',

        }),
        new MiniCssExtractPlugin({
            filename: 'css/[name].[hash:6].css',
        }),
        new CopyWebpackPlugin(
            [
                {from: './assets/img', to: 'img'},
            ],
            {copyUnmodified: false}
        ),
    ],
};

module.exports = config;
