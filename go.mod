module bitbucket.org/vivhealth/viv-admin-mvp

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/aws/aws-sdk-go v1.17.5
	github.com/gorilla/mux v1.7.0
	github.com/pborman/uuid v1.2.0
	github.com/pkg/errors v0.8.1
	github.com/spf13/pflag v1.0.3
	github.com/spf13/viper v1.3.1
	github.com/stretchr/testify v1.3.0 // indirect
	github.com/thedevsaddam/renderer v1.2.0
	//	github.com/thedevsaddam/renderer v0.0.0-20181219102759-871aa24663b6
	go.uber.org/atomic v1.3.2 // indirect
	go.uber.org/multierr v1.1.0 // indirect
	go.uber.org/zap v1.9.1
	golang.org/x/net v0.0.0-20190225153610-fe579d43d832 // indirect
)
