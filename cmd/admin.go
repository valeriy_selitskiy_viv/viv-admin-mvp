package main

import (
	"log"
	"net/http"

	"bitbucket.org/vivhealth/viv-admin-mvp/core"
	"bitbucket.org/vivhealth/viv-admin-mvp/core/logger"
	"bitbucket.org/vivhealth/viv-admin-mvp/internal/aws/s3"
	"bitbucket.org/vivhealth/viv-admin-mvp/internal/config"
	"bitbucket.org/vivhealth/viv-admin-mvp/internal/filesystem"
	"bitbucket.org/vivhealth/viv-admin-mvp/internal/handler"
	"bitbucket.org/vivhealth/viv-admin-mvp/internal/httpadapter"
	"bitbucket.org/vivhealth/viv-admin-mvp/internal/httpadapter/wrapper"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	awss3 "github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/gorilla/mux"
	"github.com/spf13/pflag"
)

func main() {
	cfg := readConfig()
	r := mux.NewRouter()

	// STATIC
	fs := wrapper.TemplateRenderer(http.FileServer(filesystem.NonListable{
		FS: http.Dir(wrapper.DistPath),
	}))

	// API
	api := r.PathPrefix("/api").Subrouter()
	api.Use(func(handler http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Content-Type", "application/json")
			ctx := logger.ToContext(r.Context(), logger.Must(logger.NewLogger(nil)))
			ctx = logger.PopulateContextWithRequestID(ctx, core.NewID())
			handler.ServeHTTP(w, r.WithContext(ctx))
		})
	})
	service := newHandlerService(cfg)
	adapter := httpadapter.New(service)
	api.HandleFunc("/programs", adapter.GetPrograms).Methods("GET")
	api.HandleFunc("/categories", adapter.GetCategories).Methods("GET")
	api.PathPrefix("/program").HandlerFunc(adapter.GetProgram).Methods("GET")
	api.PathPrefix("/program").HandlerFunc(adapter.PatchProgram).Methods("PATCH")

	// ROOT
	r.PathPrefix("/").Handler(fs)

	port := ":3000"
	log.Println("Listening on port", port)
	log.Fatal(http.ListenAndServe(port, r))
}

func newHandlerService(cfg config.Config) *handler.Service {
	s3Session := session.Must(session.NewSession(aws.NewConfig().WithRegion(cfg.S3.Region)))
	awsDataSource := s3.NewClient(awss3.New(s3Session), s3manager.NewDownloader(s3Session, func(d *s3manager.Downloader) {
		d.Concurrency = 15
	}), s3manager.NewUploader(s3Session), cfg.S3.ContentBucketName)

	return &handler.Service{
		Cfg: cfg,

		DataSource: awsDataSource,
	}
}

func readConfig() config.Config {
	pflag.Parse()

	src, err := config.NewSource()
	if err != nil {
		panic(err)
	}

	var cfg config.Config
	if err := src.ReadConfig(&cfg); err != nil {
		panic(err)
	}

	return cfg
}
