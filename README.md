# VIV Content admin system

## Installation:

### Prerequisites:
- AWS CLI and profile
- GoLang 1.11 or upper
- NPM 

### Setup project:
```
git clone git@bitbucket.org:vivhealth/viv-admin-mvp.git
cd viv-admin-mvp
direnv allow
go mod download

cd resources 
npm install
sudo npm install -g webpack webpack-cli
npm run build
```

### Setup IDE (GoLand):
1. Create new run configuration from go build template
    - file to build: `PROJECT_DIR/cmd/admin.go`
    - working directory: `PROJECT_DIR`
    - add env variable: `AWS_PROFILE=dev`
    
2. Enable **go modules** support in IDE settings

3. Add file watcher from custom template (rebuilds js bundle on save):
    - select `Any` file type
    - create new scope, f.e. `frontend-watch` with pattern:
        - `(file[viv-admin-mvp]:resources/src/*||file[viv-admin-mvp]:resources/src//*)&&!file:resources/src/layout.html`
    - set binary to `npm`
    - set arguments to `run build`
    - set `resources` as working dierectory
    - disable auto-save in advanced options
    
## Usage:
1. Run in IDE or by `go run`
2. Open [Project URL](http://127.0.0.1:3000) in browser